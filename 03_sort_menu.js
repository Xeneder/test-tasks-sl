/**
 * Функция, сортирующая выбранные пункты основного меню в порядке убывания их приоритета. Отсортированные пункты вставляются в начало списка. Не найденные пункты игнорируются.
 * @param {Object} orderHash - хеш, задающий порядок сортировки, где ключами являются надписи в меню, а значениями - их приоритет.
 */
function sortMenu(orderHash) {
    Object.entries(orderHash)
        .sort(function(a, b) {
            return a[1] - b[1]
        })
        .forEach(function(element) {
            $oldMenuElement = $(
                '.tcp-category-list__item:contains(' + element[0] + ')'
            )
            $newMenuElement = $oldMenuElement.clone(true)
            $('.tcp-category-list').prepend($newMenuElement)
            $oldMenuElement.remove()
        })
}

var affinity = {
    categories: {
        'Бытовая техника': 518,
        Диваны: 374,
        'Компьютерная и оргтехника': 278,
        Двухкамерные: 384,
        'Дача и сад': 1676,
        Мебель: 401,
        'Стиральные машины': 771,
        Зоотовары: 880,
        'Диваны и кресла': 374,
        'Прямые диваны': 373,
    },
}

sortMenu(affinity.categories)
