/**
 * Функция прикрепляет кнопку "купить" к нижнему левому краю страницы.
 * @param {string} direction - указывает направление ('right'/'top') в сторону которого будет подвинута кнопка (относительно блока "номер сессии"). Занчение по-умолчанию - 'right', однако тогда на кнопку залезает виджет чата.
 */
function stickBuyButtonToBottom(direction) {
  if (direction === undefined) {
    direction = 'right';
  }

  var $buyButton = $('#item_button_buy')
    .parent()
    .clone(true);
  $('#item_button_buy').remove();

  $buyButton
    // Не смотря на то, что семантически это неправильно,
    // добавляем класс для стилистически похожего поведения
    // двух соседних элементов (в основном из-за медиа-запросов)
    .addClass('tcp-nuid')
    .css({
      height: 38,
      paddingLeft: 140 * (direction === 'right'),
      paddingBottom: 90 * (direction === 'top'),
      background: 'none',
      borderColor: 'transparent',
      boxShadow: 'none'
    });
  $buyButton.find('#item_button_buy').css({
    height: 38,
    width: (direction === 'top' && 140) || 'auto'
  });

  // Добавляем в конец страницы во избежание проблем с z-index
  $('.tcp-nuid').after($buyButton);
}
