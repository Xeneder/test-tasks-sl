/**
 * Самая простая функция — находит количество товаров из DOM-дерева. Работает даже при динамической подгрузке через фильтр.
 * @param {Object} $searchElement - jQuery объект, внутри которого будет осуществлен поиск.
 */
function simpleGetProductQuantity($searchElement) {
  if ($searchElement === undefined) {
    $searchElement = $(document);
  }
  return parseInt($searchElement.find('.view-order-qnt').text());
}

/**
 * Менее тривиальная функция — использует dataLayer. Подходит только для получения количества товаров без динамической подгрузки страницы.
 */
function dataLayerGetProductQuantity() {
  return +dataLayer[0].productsQuantityAvailable;
}

/**
 * Самая нестандартная функция — асинхронно работает с любой страницы, отправляя параметр в callback-функцию. Работает через парсинг "облегченной" версии тела страницы.
 */
function asyncGetProductQuantity(callback) {
  $.ajax({
    url: 'https://boscooutlet.ru/catalog/men/obuv_1/?only_body=Y',
    success: function(data) {
      callback(simpleGetProductQuantity($(data)));
    }
  });
}
